package com.pascal.drawingapplication

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.graphics.BlurMaskFilter




/**
 * Created by User on 7/10/2018.
 */
class DrawingView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private var drawPath: Path? = null
    private var backgroundPaint: Paint? = null
    private var drawPaint: Paint? = null
    private var drawCanvas: Canvas? = null
    private var canvasBitmap: Bitmap? = null

    private var previousTouchX = -1f
    private var previousTouchY = -1f

    private val paths: MutableList<Path> = ArrayList<Path>()
    private val paints: MutableList<Paint> = ArrayList<Paint>()
    private val undonePaths: MutableList<Path> = ArrayList<Path>()
    private val undonePaints: MutableList<Paint> = ArrayList<Paint>()

    // Set default values
    private var backgroundColor = -0x1
    private var paintColor = -0x9a0000
    private var strokeWidth = 10

    val bitmap: Bitmap?
        get() {
            drawBackground(drawCanvas)
            drawPaths(drawCanvas)
            return canvasBitmap
        }

    init {
        init()
    }

    private fun init() {
        drawPath = Path()
        backgroundPaint = Paint()
        initPaint()
    }

    private fun initPaint() {
        drawPaint = Paint()
        with(drawPaint!!) {
            color = paintColor
            isAntiAlias = true
            strokeWidth=this@DrawingView.strokeWidth.toFloat()
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
        }
    }

    private fun drawBackground(canvas: Canvas?) {
        backgroundPaint?.color = backgroundColor
        backgroundPaint?.style = Paint.Style.FILL
        canvas?.drawRect(0f, 0f, width.toFloat(), height.toFloat(), backgroundPaint)
    }

    private fun drawPaths(canvas: Canvas?) {


        for ((i, p) in paths.withIndex()) {
            paints[i].maskFilter = BlurMaskFilter(6f, BlurMaskFilter.Blur.NORMAL)
            canvas?.drawPath(p, paints[i])
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawBackground(canvas)
        drawPaths(canvas)
        drawPaint?.maskFilter = BlurMaskFilter(4f, BlurMaskFilter.Blur.NORMAL)
        canvas.drawPath(drawPath, drawPaint)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)

        drawCanvas = Canvas(canvasBitmap)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val touchX = event.x
        val touchY = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                drawPath?.moveTo(touchX, touchY)
                setPreviousTouch(touchX, touchY)
            }
            MotionEvent.ACTION_MOVE -> {
                if (touchX != -1f && touchY != -1f) {
                    drawPath?.quadTo(previousTouchX, previousTouchY, touchX, touchY)
                } else {
                    drawPath?.lineTo(touchX, touchY)
                }
                setPreviousTouch(touchX, touchY)
            }
            MotionEvent.ACTION_UP -> {
                drawPath?.lineTo(touchX, touchY)
                paths.add(drawPath as Path)
                paints.add(drawPaint as Paint)
                setPreviousTouch(-1f, -1f)
                drawPath = Path()
                initPaint()
            }
            else -> return false
        }

        invalidate()
        return true
    }

    private fun setPreviousTouch(touchX: Float, touchY: Float) {
        previousTouchX = touchX
        previousTouchY = touchY
    }

    fun clearCanvas() {
        paths.clear()
        paints.clear()
        undonePaths.clear()
        undonePaints.clear()
        drawCanvas?.drawColor(0, PorterDuff.Mode.CLEAR)
        invalidate()
    }

    fun setPaintColor(color: Int) {
        paintColor = color
        drawPaint?.color = paintColor
    }

    fun setPaintStrokeWidth(strokeWidth: Int) {
        this.strokeWidth = strokeWidth
        drawPaint?.strokeWidth = strokeWidth.toFloat()
    }

    override fun setBackgroundColor(color: Int) {
        backgroundColor = color
        backgroundPaint?.color = backgroundColor
        this.invalidate()
    }

    fun undo() {
        if (paths.size > 0) {
            undonePaths.add(paths.removeAt(paths.size - 1))
            undonePaints.add(paints.removeAt(paints.size - 1))
            invalidate()
        }
    }

    fun redo() {
        if ((undonePaths.size) > 0) {
            paths.add(undonePaths.removeAt(undonePaths.size - 1))
            paints.add(undonePaints.removeAt(undonePaints.size - 1))
            invalidate()
        }
    }
}