package com.pascal.drawingapplication

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.xdty.preference.colorpicker.ColorPickerDialog


class MainActivity : AppCompatActivity() {
    private var mCurrentColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        initDrawingView()
        changeColorButton.setOnClickListener {
            val colors = resources.getIntArray(R.array.palette)

            val dialog = ColorPickerDialog.newInstance(R.string.color_picker_default_title,
                    colors,
                    mCurrentColor,
                    5,
                    ColorPickerDialog.SIZE_SMALL)

            dialog.setOnColorSelectedListener { color ->
                mCurrentColor = color
                drawingView.setPaintColor(mCurrentColor)
            }

            dialog.show(fragmentManager, "ColorPickerDialog")
        }
        undoButton.setOnClickListener { drawingView.undo() }
        redoButton.setOnClickListener { drawingView.redo() }
    }

    private fun initDrawingView() {
        drawingView.setPaintColor(Color.GRAY)
        drawingView.setPaintStrokeWidth(50)
    }
}
